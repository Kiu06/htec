# HTEC FE TEST

Repository for HTEC FE test.
Built width ReactJS, Sass, Bootstrap.

## API

In order to use the API you will need to generate API key from https://newsapi.org/.
Add it to local .env file and name it like this:

```
REACT_APP_API_KEY=YOUR_API_KEY
```

## Available Scripts

In the project directory, you can run:

```
$ npm i
$ npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

