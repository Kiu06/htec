/**
 * Parse env object
 */
function parseEnv() {
    const env_obj = {};
  
    for (let key in process.env)
      env_obj[key.replace("REACT_APP_", "")] = process.env[key];
  
    return env_obj;
}

export {
    parseEnv
}