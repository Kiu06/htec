import React, { useEffect, useContext } from 'react';
import Consumer, { MyContext } from "./../Provider";
import Item from '../../partials/item/Item';
import Loader from '../../partials/loader/Loader';

const TopNews = () => {
    const useMyContext = useContext(MyContext);
    useEffect(() => {
        useMyContext.renderTopNews(useMyContext.state.country);
    }, [useMyContext.state.country]);
    return (
        <Consumer>
            {(ctx) => (
                <div className="topNews">
                    <h1>Top news from {ctx.state.country === 'gb' ? 'Great Britain' : 'United States'}: </h1>
                    <div className="row topNewsItemsWrap">
                        {ctx.state.loading ?
                            <Loader />
                            :
                            ctx.state.items.slice(0, ctx.state.itemsCount).map(item => (
                                <Item key={item.url} data={item} />
                            ))
                        }
                    </div>
                    {ctx.state.loading ? '' : <button className="loadMore" onClick={() => ctx.loadMore()}>Load More</button>}
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(TopNews);