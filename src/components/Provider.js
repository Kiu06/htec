import React, { createContext } from 'react';
import { parseEnv } from "../utils/utils.js";
import API from "../utils/API.js";
import '../scss/styles.scss';
const MyContext = createContext();
const Provider = MyContext.Provider;
const Consumer = MyContext.Consumer;

class ConfigProvider extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      loading: true,
      country: 'gb',
      categories: [
        { id: 0, name: 'business', items: [], totalResults: 5 },
        { id: 1, name: 'entertainment', items: [], totalResults: 5 },
        { id: 2, name: 'general', items: [], totalResults: 5 },
        { id: 3, name: 'health', items: [], totalResults: 5 },
        { id: 4, name: 'science', items: [], totalResults: 5 },
        { id: 5, name: 'sport', items: [], totalResults: 5 },
        { id: 6, name: 'technology', items: [], totalResults: 5 }
      ],
      category: 'business',
      disabled: false,
      items: [],
      itemsCount: 6,
      searchfrase: '',
      searchItems: [],
      error: ''
    }
    this.env = parseEnv();
  }

  async componentDidMount() {
    let country = JSON.parse(localStorage.getItem("country"));
    if (country) {
      this.renderTopCategories(country, this.state.category)
    } else {
      this.renderTopCategories(this.state.country, this.state.category)
    }
  }

  renderTopCategories = async (country, category) => {
    for (var i = 0; i < this.state.categories.length; i++) {
      try {
        this.setState({ loading: true });
        this.renderCategory(country, this.state.categories[i].name, i)
      } catch (error) {
        console.log(error);
        this.setState({ error })
      }
    }
  }

  renderCategory = async (country, category, id) => {
    try {
      let res = await API.get(`/top-headlines?country=${country}&category=${category}&apiKey=${this.env.API_KEY}`);
      if (res.status === 200) {
        let catItems = await res.data.articles;
        let totalResults = await res.data.totalResults;
        let catArray = [...this.state.categories];
        catArray[id] = { ...catArray[id], items: catItems, totalResults };
        this.setState({ categories: catArray, disabled: false, error: '', loading: false })
      }
    } catch (error) {
      console.log(error);
      this.setState({ error })
    }
  }

  renderTopNews = async (country) => {
    try {
      let res = await API.get(`/top-headlines?country=${country}&sortBy=publishedAt&apiKey=${this.env.API_KEY}`);
      if (res.status === 200) {
        let items = await res.data.articles;
        this.setState({ items, disabled: false, itemsCount: 6, error: '', loading: false })
      }
    } catch (error) {
      console.log(error);
      this.setState({ error })
    }
  }

  countryChangeHandler = async (e) => {
    let value = e.target.value;
    if (this.state.country !== value) {
      this.setState({ loading: true, country: value, disabled: true },
        () => {
          if (this.state.searchfrase !== '') this.search(this.state.searchfrase, this.state.country)
          localStorage.setItem("country", JSON.stringify(value));
          this.renderTopCategories(value, this.state.category)
        });
    }
  }

  search = async (val, country) => {
    if (val !== '') {
      this.setState({ loading: true });
      try {
        let res = await API.get(`/top-headlines?q=${val}&country=${country}&sortBy=publishedAt&apiKey=${this.env.API_KEY}`);
        if (res.status === 200) {
          let searchItems = await res.data.articles;
          this.setState({ searchItems, disabled: false, loading: false })
        }
      } catch (error) {
        console.log(error);
        this.setState({ error })
      }
    } else {
      this.clearSearch();
    }
  };

  onChangeHandler = async e => {
    if (e.target.value !== '') {
      this.search(e.target.value, this.state.country);
      this.setState({ searchfrase: e.target.value });
    } else {
      this.clearSearch();
    }
  };

  onSubmitHandler = async e => {
    e.preventDefault();
    if (e.target.value !== '') {
      this.search(e.target.searchprase.value, this.state.country);
      this.setState({ searchfrase: e.target.searchprase.value });
    } else {
      this.clearSearch();
    }
  }

  clearSearch = () => {
    this.setState({ loading: false, searchfrase: '', searchItems: [], disabled: false })
  }

  render() {
    if (this.state.error) {
      return (
        <div>
          <p>{this.state.error}</p>
        </div>
      )
    } else {
      return (
        <Provider value={{
          state: this.state,
          countryChangeHandler: (e) => this.countryChangeHandler(e),
          onChangeHandler: (e) => this.onChangeHandler(e),
          onSubmitHandler: (e) => this.onSubmitHandler(e),
          loadMore: () => this.setState(prevState => { return { itemsCount: prevState.itemsCount + 6 } }),
          toggleLangDisabled: (val) => this.setState({ disabled: val }),
          renderTopCategories: (country, category) => this.renderTopCategories(country, category),
          renderCategory: (country, category, id) => this.renderCategory(country, category, id),
          renderTopNews: (country) => this.renderTopNews(country),
          clearSearch: () => this.clearSearch()
        }}>
          {this.props.children}
        </Provider>
      );
    }
  }
}

export { MyContext, Provider, ConfigProvider };
export default Consumer;
