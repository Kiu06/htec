import React from 'react';
import { animateScroll as scroll } from "react-scroll";

const Footer = () => {
    function scrollToTop() {
        scroll.scrollToTop({
            duration: '500',
            smooth: true
        });
    }
    return (
        <div id="footer">
            <button onClick={scrollToTop} id="scrollToTopBtn">{'Back to top'}</button>
        </div>
    )
}

export default Footer;