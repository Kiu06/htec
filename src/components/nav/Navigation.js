import React from 'react';
import { NavLink } from 'react-router-dom';
import Consumer from "../Provider";
import CountrySelect from "../../partials/countrySelect/CountrySelect";

const Navigation = () => {
  function scrollToTop() {
    window.scrollTo(0, 0);
  }

  return (
    <Consumer>
      {(ctx) => (
        <nav id="nav">
          <ul>
            <NavLink to="/" exact onClick={scrollToTop}><li>Top News</li></NavLink>
            <NavLink to="/categories" onClick={scrollToTop}><li>Categories</li></NavLink>
            <NavLink to="/search" onClick={scrollToTop}><li>Search</li></NavLink>
          </ul>
          <CountrySelect />
        </nav>
      )}
    </Consumer>
  );
}

export default Navigation;