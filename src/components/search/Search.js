import React, { useEffect, useContext } from 'react';
import Consumer, { MyContext } from "./../Provider";
import Item from '../../partials/item/Item';
import Loader from '../../partials/loader/Loader';

const Search = () => {
    const useMyContext = useContext(MyContext);

    useEffect(() => {
        return () => {
            useMyContext.clearSearch();
        }
    }, []);

    return (
        <Consumer>
            {(ctx) => (
                <div className="search">
                    <form onSubmit={(e) => ctx.onSubmitHandler(e)}>
                        <h1>Search top news from {ctx.state.country === "gb" ? "Great Britain" : "United States"}</h1>
                        <input
                            type="text"
                            placeholder="Search term..."
                            name="searchprase"
                            value={ctx.state.searchpfrase}
                            onChange={(e) => ctx.onChangeHandler(e)}
                        />
                    </form>

                    <div className="row searchItemsWrap">
                        {ctx.state.loading ?
                            <Loader />
                            :
                            ctx.state.searchItems.map(item => (
                                <Item key={item.url} data={item} />
                            ))
                        }
                    </div>
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(Search);