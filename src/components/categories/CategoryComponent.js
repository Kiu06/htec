import React, { useState } from 'react';
import Consumer from "./../Provider";
import SliderComponent from "../../partials/slider/SliderComponent";
import Item from "../../partials/item/Item";
import Loader from "../../partials/loader/Loader";
import { Link } from 'react-router-dom';

const CategoryComponent = (props) => {
    const [catItemsCount, setCatItemsCount] = useState(5);

    function toggleExpand() {
        if (catItemsCount === 5) {
            setCatItemsCount(props.cat.catItemsCount)
        } else {
            setCatItemsCount(5);
        }
    }

    return (
        <Consumer>
            {(ctx) => (
                <div className="sliderWrap">
                    {ctx.state.loading ?
                        <Loader />
                        :
                        <React.Fragment>
                            <h2>
                                <Link to={`/categories/${props.cat.id}/${props.cat.name}`}>{props.cat.name}</Link>
                                <button onClick={toggleExpand}>{catItemsCount === 5 ? "expand" : "collapse"}</button>
                            </h2>
                            <div className={catItemsCount === 5 ? "collapsed" : "row expanded"}>
                                {catItemsCount === 5
                                    ? <SliderComponent data={props.cat.items} />
                                    : props.cat.items.map(catItem => (
                                        <Item key={catItem.url} data={catItem} />
                                    ))
                                }
                            </div>
                        </React.Fragment>
                    }
                </div>

            )}
        </Consumer>
    )
}

export default CategoryComponent;