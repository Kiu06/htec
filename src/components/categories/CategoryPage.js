import React, { useContext, useEffect } from 'react';
import Consumer, { MyContext } from "./../Provider";
import Item from '../../partials/item/Item';
import Loader from '../../partials/loader/Loader';

const CategoryPage = ({ match }) => {
    const useMyContext = useContext(MyContext);

    useEffect(() => {
        window.scrollTo(0, 0);
        useMyContext.renderCategory(useMyContext.state.country, match.params.name, match.params.id);
    }, [useMyContext.state.country])

    return (
        <Consumer>
            {(ctx) => (
                <div className="categoryPage">
                    <h1>Top {match.params.name} News from {ctx.state.country === 'gb' ? 'Great Britain' : 'United States'}</h1>
                    <div className="row">
                        {ctx.state.loading
                            ? <Loader />
                            : ctx.state.categories[match.params.id].items.map(item => (
                                <Item key={item.url} data={item} />
                            ))
                        }
                    </div>
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(CategoryPage);