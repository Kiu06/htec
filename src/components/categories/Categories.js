import React from 'react';
import Consumer from "./../Provider";
import CategoryComponent from './CategoryComponent';
import Loader from '../../partials/loader/Loader';

const Categories = () => {
    return (
        <Consumer>
            {(ctx) => (
                <div className="categories">
                    <h1>Top 5 News by categories from {ctx.state.country === 'gb' ? 'Great Britain' : 'United States'}</h1>
                    {ctx.state.loading ?
                        <Loader />
                        :
                        ctx.state.categories.map(item => (
                            <CategoryComponent key={item.id} cat={item} />
                        ))
                    }
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(Categories);