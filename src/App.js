import React, { Suspense, lazy } from 'react';
import Navigation from './components/nav/Navigation';
import TopNews from './components/topNews/TopNews';
import CategoryPage from './components/categories/CategoryPage';
import Article from './partials/article/Article';
import Search from './components/search/Search';
import Footer from './components/footer/Footer';
import Loader from './partials/loader/Loader';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ConfigProvider } from "./components/Provider";
import './scss/styles.scss';

const Categories = lazy(() => import("./components/categories/Categories"));

function App() {
  return (
    <ConfigProvider>
      <Router>
        <div className="container App">
          <Navigation />
          <Suspense fallback={
            <Loader />
          }>
            <Switch>
              <Route path='/' exact component={TopNews} />
              <Route path='/search' component={Search} />
              <Route path='/news/article' exact component={Article} />
              <Route path='/categories' exact component={Categories} />
              <Route path='/categories/:id/:name' exact component={CategoryPage} />
            </Switch>
          </Suspense>
          <Footer />
        </div>
      </Router>
    </ConfigProvider>
  );
}

export default App;