import React from 'react';
import Consumer from "../../components/Provider";
import { useHistory } from 'react-router-dom';

const Item = (props) => {
    const history = useHistory();

    function handleClick() {
        history.push(`/news/article`);
    }

    return (
        <Consumer>
            {(ctx) => (
                <div className="itemWrap col-12 col-sm-6 col-md-4" onClick={handleClick}>
                    <h3>{props.data.title}</h3>
                    {props.data.urlToImage
                        ? <img src={props.data.urlToImage} alt={props.data.url} />
                        : <img src="../../images/placeholder-img.jpg" alt="placeholder-img" />
                    }
                    <p>{props.data.description}</p>
                    <span>More ></span>
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(Item);