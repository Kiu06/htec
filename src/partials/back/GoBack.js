import React from 'react';
import { useHistory } from 'react-router-dom'

const GoBack = () => {
    let history = useHistory()
    return (
        <button onClick={() => { history.goBack() }} className="backBtn">{'< Back to list'}</button>
    )
}

export default GoBack;