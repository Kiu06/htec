import React, { useEffect, useContext } from 'react';
import Consumer, { MyContext } from "../../components/Provider";
import GoBack from '../back/GoBack';

const Article = () => {
    const useMyContext = useContext(MyContext);

    useEffect(() => {
        //component did mount
        useMyContext.toggleLangDisabled(true);
        window.scrollTo(0, 0);
        return () => {
            //component will unmount
            useMyContext.toggleLangDisabled(false);
        }
    }, []);

    useEffect(() => {
        //component will receive props
        useMyContext.toggleLangDisabled(true);
    }, [useMyContext.state.disabled])

    return (
        <Consumer>
            {(ctx) => (
                <div className="article">
                    <h1>Title</h1>
                    <img src="../../../images/placeholder-img.jpg" alt="placeholder-img" />
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla maximus elit nec varius. Nullam a nunc elementum, fringilla lectus non, viverra velit. Fusce tellus nulla, feugiat in pulvinar quis, scelerisque et ligula. Aliquam vitae velit ac risus tempus imperdiet quis et ex. Integer nec mattis orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Quisque aliquam, magna id mattis pellentesque, nibh mauris fringilla dolor, vel auctor leo nulla at purus.</p>
                    <GoBack />
                </div>
            )}
        </Consumer>
    )
}

export default React.memo(Article);