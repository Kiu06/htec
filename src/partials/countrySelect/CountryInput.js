import React from 'react';
import PropTypes from "prop-types";
import Consumer from "../../components/Provider";

const CountryInput = (props) => {
    const { country } = props;

    return (
        <Consumer>
            {(ctx) => (
                <label className={`${ctx.state.country === country ? 'active' : ''} 
                    ${ctx.state.disabled || ctx.state.loading ? "disabled" : ""}`}>
                    <input
                        type="radio"
                        name="country"
                        value={country}
                        checked={ctx.state.country === country}
                        onChange={(e) => ctx.countryChangeHandler(e)}
                        disabled={ctx.state.disabled}
                    />
                    <span className="noselect">{country}</span>
                </label>
            )}
        </Consumer>
    )
}

CountryInput.propTypes = {
    country: PropTypes.string.isRequired,
};

export default CountryInput;