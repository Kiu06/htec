import React from 'react';
import CountryInput from "./CountryInput";

const CountrySelect = () => {
    return (
        <form id="langWrap">
            <CountryInput country="gb" />
            <CountryInput country="us" />
        </form>
    )
}

export default CountrySelect;