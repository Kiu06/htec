import React, { useState, useEffect } from 'react';
import Consumer from "../../components/Provider";
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { useHistory } from 'react-router-dom';

const SliderComponent = (props) => {
    const [visibleSlides, setVisibleSlides] = useState(3);
    const [naturalSlideHeight, setNaturalSlideHeight] = useState(130);
    const history = useHistory();

    useEffect(() => {
        setVisibleSlidesBasedOnWindowSize();
        window.addEventListener('resize', setVisibleSlidesBasedOnWindowSize(), false);
    }, [])

    function setVisibleSlidesBasedOnWindowSize() {
        if (document.body.clientWidth >= 720) {
            setVisibleSlides(3);
            setNaturalSlideHeight(100);
        } else if (document.body.clientWidth >= 500) {
            setVisibleSlides(2);
            setNaturalSlideHeight(160);
        } else {
            setVisibleSlides(1)
            setNaturalSlideHeight(130);
        }
    };

    function handleClick() {
        history.push(`/news/article`);
    }

    return (
        <Consumer>
            {(ctx) => (
                <CarouselProvider
                    naturalSlideWidth={100}
                    naturalSlideHeight={naturalSlideHeight}
                    totalSlides={5}
                    visibleSlides={visibleSlides}
                    infinite={true}
                >
                    <Slider>
                        {props.data.slice(0, 5).map((slide, i) => (
                            <Slide key={slide.url} className="slideWrap" index={i} onClick={handleClick}>
                                <h3>{slide.title}</h3>
                                {slide.urlToImage
                                    ? <img src={slide.urlToImage} alt={slide.url} />
                                    : <img src="./images/placeholder-img.jpg" alt="placeholder-img" />
                                }
                                <p>{slide.description}</p>
                                <span>More ></span>
                            </Slide>
                        ))}
                    </Slider>
                    <div className="buttons-wrap">
                        <ButtonBack children={true}>
                            <span></span>
                        </ButtonBack>
                        <ButtonNext children={true}>
                            <span></span>
                        </ButtonNext>
                    </div>
                </CarouselProvider>
            )}
        </Consumer>
    )
}

export default React.memo(SliderComponent);